import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
    state : {
      // Fake user data for testing
        userData: [
            {title:"Go shopping",
             text:"Go shopping with mama at 4:00PM",
             DDate:new Date(119,11,18),
             isFinished:false,
             EventID:0},
             {title:"Go Dinner with papa",
             text:"@siam paragon 5th floor 6:00PM",
             DDate:new Date(119,11,18),
             isFinished:false,
             EventID:1}
        ],
        deletedUserData: [],
        currentDate:new Date(119,11,18)
    },
    getters:{
        getTodayUserData(state) {
            return state.userData.filter((obj)=>{

                return JSON.stringify(obj.DDate) 
                === 
                JSON.stringify(state.currentDate);
            })
        },
        getLaterDaysUserData(state) {
            return state.userData.filter((obj)=>{
                return obj.DDate
                >
                state.currentDate;
            })
        }
    },
    mutations:{
        AddToUserData(state,obj) {
            state.userData.push(obj);
        }
    }
})